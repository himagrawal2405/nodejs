const request = require('request');
const forecast = (lattitude, longitude, callback) => {
  const url =
    'http://api.weatherstack.com/current?access_key=486576ee205743fd2839390e1284c7f5&query=' +
    lattitude +
    ',' +
    longitude;
  request({ url, json: true }, (error, { body }) => {
    if (error) {
      callback('Unable to connect to location services!', undefined);
    } else if (body.current.length === 0) {
      callback('Unable to find location!', undefined);
    } else {
      callback(undefined, {
        weather_description: body.current.weather_descriptions[0],
        current_temperature: body.current.temperature,
        feelslike: body.current.feelslike,
      });
    }
  });
};
module.exports = forecast;
