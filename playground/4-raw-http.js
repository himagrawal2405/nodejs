const http = require('http');
const url =
  'http://api.weatherstack.com/current?access_key=486576ee205743fd2839390e1284c7f5&query=40,-75';

const request = http.request(url, (response) => {
  let data = '';
  response.on('data', (chunk) => {
    data = data + chunk.toString();
  });

  response.on('end', () => {
    const body = JSON.parse(data);
    console.log(body);
  });
});

request.on('error', (error) => {
  console.log('An error ', error);
});

request.end();
