console.log('Client side js file is loaded');

const weatherForm = document.querySelector('form');
const search = document.querySelector('input');
const messageOne = document.querySelector('#message-1');
const messageTwo = document.querySelector('#message-2');

weatherForm.addEventListener('submit', (e) => {
  e.preventDefault();
  const location = search.value;
  messageOne.textContent = 'Loading...';
  messageTwo.textContent = '';
  fetch('/weather?address=' + location).then((response) => {
    response.json().then((data) => {
      if (data.error) {
        console.log('error ', data.error);
        messageOne.textContent = 'Error: ' + data.error;
      } else {
        console.log(data.forecast);
        messageOne.textContent =
          'Weather: ' + data.forecast.weather_description;
        messageTwo.textContent =
          'Current Temperature: ' + data.forecast.current_temperature;
      }
    });
  });
});
