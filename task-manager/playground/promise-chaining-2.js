require('../src/db/mongoose');
const Task = require('../src/models/tasks');

// Task.findByIdAndDelete('5ee4c45a2457d002a83ba336')
//   .then(() => {
//     console.log('Deleted');
//     return Task.countDocuments({ completed: false });
//   })
//   .then((result) => console.log(result))
//   .catch((e) => console.log(e));

const deleteTaskAndCount = async (id) => {
  const task = await Task.findByIdAndDelete(id);
  const count = await Task.countDocuments({ completed: false });
  return count;
};

deleteTaskAndCount('5ed4ba903dec27577422f072')
  .then((count) => console.log(count))
  .catch((e) => console.log(e));
