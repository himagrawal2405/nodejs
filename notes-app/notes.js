const fs = require('fs');
const chalk = require('chalk');
const getNotes = () => 'Your notes....';

const addNote = (title, body) => {
  const notes = loadNotes();
  const duplicateNote = notes.find((note) => note.title === title);
  if (!duplicateNote) {
    notes.push({
      title: title,
      body: body,
    });
    saveNotes(notes);
  } else {
    console.log('Note title taken');
  }
};

const removeNote = (title) => {
  const notes = loadNotes();
  const noteToKeep = notes.filter((w) => w.title !== title);
  if (noteToKeep.length < notes.length) {
    saveNotes(noteToKeep);
    return true;
  } else {
    return false;
  }
};

const saveNotes = (notes) => {
  const dataJSON = JSON.stringify(notes);
  fs.writeFileSync('notes.json', dataJSON);
};

const loadNotes = () => {
  try {
    const dataBuffer = fs.readFileSync('notes.json');
    const dataJSON = dataBuffer.toString();
    return JSON.parse(dataJSON);
  } catch (e) {
    return [];
  }
};

const listNote = () => {
  try {
    const dataObj = loadNotes();
    dataObj.forEach((data) => {
      console.log(chalk.green.bold(data.title));
    });
  } catch (e) {
    return [];
  }
};

const readNote = (title) => {
  try {
    const notes = loadNotes();
    const note = notes.find((note) => note.title === title);
    debugger;
    //windows: node --inspect-brk app.js --title="Title" --body="node.js"
    //mac: node inspect app.js --title="Title" --body="node.js"
    //chrome://inspect
    if (note) {
      console.log(chalk.green.bold(note.title));
      console.log(chalk.blue(note.body));
    } else {
      console.log(chalk.red.bold('No note found'));
    }
  } catch (e) {
    return [];
  }
};

module.exports = {
  getNotes: getNotes,
  addNote: addNote,
  removeNote: removeNote,
  listNote: listNote,
  readNote: readNote,
};
