const validator = require('validator');
const chalk = require('chalk');
const yargs = require('yargs');
// const add = require('./util');
const notes = require('./notes');

// console.log(validator.isEmail('sudipgh@gmail.com'));
// console.log(validator.isURL('http:/google.com'));
// console.log(chalk.red.inverse.bold('Error!!'));
// console.log(process.argv[2]);

// console.log(process.argv);

//Customize yargs version
yargs.version('1.1.2');

//Create add command
yargs.command({
  command: 'add',
  describe: 'Add a new note',
  builder: {
    title: {
      describe: 'Note title',
      demandOption: true, // to mandatory pass the title option in arguments
      type: 'string', //default is boolean
    },
    body: {
      describe: 'body',
      demandOption: true,
      type: 'string',
    },
  },
  handler: (argv) => {
    notes.addNote(argv.title, argv.body);
  },
});

//Create remove command
yargs.command({
  command: 'remove',
  describe: 'Remove a note',
  builder: {
    title: {
      describe: 'Note title',
      demandOption: true, // to mandatory pass the title option in arguments
      type: 'string', //default is boolean
    },
  },
  handler: (argv) => {
    if (notes.removeNote(argv.title)) {
      console.log(chalk.green.inverse.bold('Note Removed'));
    } else {
      console.log(chalk.red.inverse.bold('No note found!!'));
    }
  },
});

//Create list command
yargs.command({
  command: 'list',
  describe: 'Lists the note',
  handler: () => notes.listNote(),
});

//Create remove command
yargs.command({
  command: 'read',
  describe: 'Read a note',
  builder: {
    title: {
      describe: 'Note title',
      demandOption: true, // to mandatory pass the title option in arguments
      type: 'string', //default is boolean
    },
  },
  handler: (argv) => notes.readNote(argv.title),
});

yargs.parse(); //this actually calls the args
